<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelBee extends Model
{
    use HasFactory;
    protected $table='bees';
    protected $fillable=['name', 'specie'];

    public function relFlower()
    {
        return $this->hasMany('App\Models\Models\ModelFlower','id_user');
    }
}
