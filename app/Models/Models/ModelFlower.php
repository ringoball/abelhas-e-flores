<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelFlower extends Model
{
    use HasFactory;
    protected $table='flowers';
    protected $fillable=['name', 'description', 'months', 'bees'];

    public function relBees()
    {
        return $this->hasMany('App\Models\Models\ModelBee','id_user');
    }
}

