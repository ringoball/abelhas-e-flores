<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       DB::insert('insert into bees(id,name,specie) values (?,?,?)', [1, 'Uruçu', 'Melipona scutellaris']);
       DB::insert('insert into bees(id,name,specie) values (?,?,?)', [2, 'Uruçu-Amarela', 'Melipona rufiventris']);
       DB::insert('insert into bees(id,name,specie) values (?,?,?)', [3, 'Guarupu', 'Melipona bicolor']);
       DB::insert('insert into bees(id,name,specie) values (?,?,?)', [4, 'Iraí', 'Nannotrigona testaceicornes']);
    }
}
