@extends('templates.template')

@section('content')
<div class="card uper">
  <div class="card-header">
    Adicionar abelha
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br/>
    @endif
      <form method="post" action="{{ route('bees.store') }}">
          <div class="form-group">
              @csrf
              <label for="country_name">Abelha:</label>
              <input type="text" class="form-control" name="name"/>
          </div>
          <div class="form-group">
              <label for="cases">Espécie:</label>
              <input type="text" class="form-control" name="specie"/>
          </div>
          <button type="submit" class="btn btn-primary">Adicionar abelha</button>
      </form>
  </div>
</div>
@endsection
