@extends('templates.template')

@section('content')

  <div class="card-header">
    Adicionar flor
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br/>
    @endif
      <form method="post" action="{{ route('flowers.store') }}">
          <div class="form-group">
              @csrf
              <label for="country_name">Nome da flor:</label>
              <input type="text" class="form-control" name="name"/>
          </div>
          <div class="form-group">
              <label for="cases">Breve descrição:</label>
              <input type="text" class="form-control" name="description"/>
          </div>
          <div class="form-group">
              <label for="cases">Mês o qual ela florece:</label>

                <input type="checkbox" name="months[]" value="jan"> Janeiro
                <input type="checkbox" name="months[]" value="fev"> Fevereiro
                <input type="checkbox" name="months[]" value="mar"> Março
                <input type="checkbox" name="months[]" value="abr"> Abril
                <input type="checkbox" name="months[]" value="mai"> Maio
                <input type="checkbox" name="months[]" value="jun"> Junho
                <input type="checkbox" name="months[]" value="jul"> Julho
                <input type="checkbox" name="months[]" value="ago"> Agosto
                <input type="checkbox" name="months[]" value="set"> Setembro
                <input type="checkbox" name="months[]" value="out"> Outubro
                <input type="checkbox" name="months[]" value="nov"> Novembro
                <input type="checkbox" name="months[]" value="dez"> Dezembro

        </div>

        <div class="form-group">
              <label for="cases">Abelhas que polinizam:</label>
              @foreach($bees as $bee)
                    <input type="checkbox" name="bees[]" value="{{$bee->id}}"> {{$bee->name}} </option>
                @endforeach
          </div>

          <button type="submit" class="btn btn-primary">Adicionar flor</button>
      </form>
  </div>

@endsection
