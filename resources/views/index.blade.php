@extends('templates.template')
@section('content')
    <h1 class="text-center">Abelhas e flores</h1><hr>
    <div class="text-center mt-3 mb-4">

    <a href="{{url('bees/create')}}">
        <button class="btn btn-success">Cadastrar abelha</button>
    </a>
    <a href="{{url('flowers/create')}}">
        <button class="btn btn-success">Cadastrar flor</button>
    </a>
    <a href="{{url('bees')}}">
        <button class="btn btn-success">Visualizar flor por abelha</button>
    </a>
</div>
@endsection
