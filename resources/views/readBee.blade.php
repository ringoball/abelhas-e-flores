@extends('templates.template')
@section('content')
    <h1 class="text-center">Abelhas e flores</h1><hr>
    <div class="text-center mt-3 mb-4">


    <div class="col-8 m-auto">
        <table class="table text-center">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Descrição</th>
            </tr>
            </thead>
            <tbody>

            <select class="form-control" name="bees">
                @foreach($bees as $bee)
                    <option value="{{$bee->id}}" name="bees">{{$bee->name}}</option>
                @endforeach
            </select><br>
            @foreach ($flowers as $flower)

                    <tr>
                        <th scope="row">{{$flower->name}}</th>
                        <td>{{$flower->description}}</td>
                    </tr>

            @endforeach
            </tbody>
        </table>
    </div>
@endsection
